import pymongo

client = pymongo.MongoClient("127.0.0.1", 27017)
db = client.test_db
print(db)

collection = db.test_collection
print(collection)

response = collection.insert_one({"test": "data", "number": 3})
print(response.inserted_id)

print(collection.find_one())
print(collection.find_one({"number": {"$gt": 1}}))
