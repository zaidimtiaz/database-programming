from sqlalchemy import create_engine
from models import Base, Student

CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(
        user="root", password="password", host="127.0.0.1", db="default"
    )
)

Base.metadata.create_all(eng)
