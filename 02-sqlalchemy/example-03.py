from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, Student

CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(
        user="root", password="password", host="127.0.0.1", db="default"
    )
)
Session = sessionmaker(bind=eng)
s = Session()

rows = s.query(Student).all()
for row in rows:
    print(row)

print("---")
total = s.query(Student).count()
print(f"Total: {total}")

print("---")
query_result = s.query(Student).filter(Student.id >= 2, Student.first_name.like("Bre%"))
print("Found students:")
for row in query_result:
    print(row)
