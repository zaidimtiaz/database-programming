import random
from datetime import datetime
from sqlalchemy.exc import IntegrityError, InvalidRequestError
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, Student, Grade

CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(
        user="root", password="password", host="127.0.0.1", db="default"
    )
)


Base.metadata.create_all(eng)
Session = sessionmaker(bind=eng)
s = Session()

s.add_all(
    [
        Grade(student=1, grade=random.randint(0, 5), date_created=datetime.now()),
        Grade(student=1, grade=random.randint(0, 5), date_created=datetime.now()),
        Grade(student=2, grade=random.randint(0, 5), date_created=datetime.now()),
        Grade(student=2, grade=random.randint(0, 5), date_created=datetime.now()),
        Grade(student=3, grade=random.randint(0, 5), date_created=datetime.now()),
        Grade(student=3, grade=random.randint(0, 5), date_created=datetime.now()),
        Grade(student=4, grade=random.randint(0, 5), date_created=datetime.now()),
        Grade(student=4, grade=random.randint(0, 5), date_created=datetime.now()),
        Grade(student=5, grade=random.randint(0, 5), date_created=datetime.now()),
        Grade(student=5, grade=random.randint(0, 5), date_created=datetime.now()),
    ]
)
s.commit()

students = s.query(Student).all()
for student_obj in students:
    grades = s.query(Grade).filter(Grade.student == student_obj.id)
    stringified_grades = ", ".join([str(g) for g in grades])
    print(f"{student_obj}: {stringified_grades}")
