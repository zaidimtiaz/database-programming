from sqlalchemy.exc import IntegrityError, InvalidRequestError
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, Student, Address

CONNECTION_STRING = "mysql+pymysql://{user}:{password}@{host}/{db}"

eng = create_engine(
    CONNECTION_STRING.format(
        user="root", password="password", host="127.0.0.1", db="default"
    )
)


Base.metadata.create_all(eng)
Session = sessionmaker(bind=eng)
s = Session()

s.add_all(
    [
        Address(student=1, street_name="Hovde", number=8, city="Kotabaru Hilir"),
        Address(student=2, street_name="Helena", number=806, city="Toledo"),
        Address(student=3, street_name="Anhalt", number=34, city="Bria"),
        Address(student=4, street_name="Nova", number=3, city="Senta"),
        Address(student=5, street_name="Walton", number=93669, city="Huayllo"),
    ]
)
s.commit()

rows = s.query(Student, Address).join(Address)
for row in rows:
    student, address = row
    print(f"{student}: {address}")
