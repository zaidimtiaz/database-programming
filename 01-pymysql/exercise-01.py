import pymysql

create_sql = """
CREATE TABLE `bikesharing` (
    `tstamp` timestamp,
    `cnt` integer,
    `temperature` decimal(5, 2),
    `temperature_feels` decimal(5, 2),
    `humidity` decimal(4, 1),
    `wind_speed` decimal(5,2),
    `weather_code` integer,
    `is_holiday` boolean,
    `is_weekend` boolean,
    `season` integer
);
"""

with pymysql.connect("127.0.0.1", "root", "password", "default") as c:
    c.execute(create_sql)
    c.execute("SELECT count(*) FROM `bikesharing`;")
    result = c.fetchone()
    if result and result[0] == 0:
        print("Succesfully created bikesharing table!")
