"""
- fetch total sum of new shares by season
- fetch total sum of new shares during thunderstorms
- fetch the date and hour with the most new shares
"""
import pymysql

seasons = {0: "spring", 1: "summer", 2: "autumn", 3: "winter"}
db = pymysql.connect("127.0.0.1", "root", "password", "default")
with db.cursor() as c:
    print("Total new shares by season:")
    c.execute("SELECT season, sum(cnt) FROM bikesharing GROUP BY season")
    for row in c.fetchall():
        print(f"{seasons[row[0]]}: {row[1]}")

    print("---------------------")

    print("Total new shares during thunderstorms:")
    c.execute("SELECT sum(cnt) FROM bikesharing WHERE weather_code=%s", (10))
    print(c.fetchone()[0])

    print("---------------------")

    print("Date and hour with most new shares:")
    c.execute("SELECT tstamp, cnt FROM bikesharing ORDER BY cnt DESC")
    date, count = c.fetchone()
    print(f"{date} with {count} shares")

db.close()
