import pymysql


DATABASE_NAME = "todo_app"
TABLE_NAME = "tasks"


def ensure_database(db):
    with db.cursor() as c:
        c.execute(
            f"CREATE SCHEMA IF NOT EXISTS `{DATABASE_NAME}` DEFAULT CHARACTER SET utf8;"
        )


def ensure_table(db):
    with db.cursor() as c:
        c.execute(
            f"""
            CREATE TABLE IF NOT EXISTS {TABLE_NAME} (
                `id` INT NOT NULL AUTO_INCREMENT,
                `task` TEXT NOT NULL,
                `done` BOOLEAN,
                PRIMARY KEY(`id`)          
            );
            """
        )


def task_list(db):
    with db.cursor() as c:
        c.execute(f"SELECT id, task FROM `{TABLE_NAME}` WHERE NOT DONE")
        for row in c.fetchall():
            id_, task = row
            print(f"#{id_}: {task}")


def add_task(db):
    text = input("Task description: ")
    if text == "":
        print("Cannot insert empty task")
        return
    with db.cursor() as c:
        c.execute(
            f"INSERT INTO `{TABLE_NAME}` (`task`, `done`) VALUES (%s, FALSE)", (text)
        )
        db.commit()


def close_task(db):
    id_ = input("Task id: ")
    with db.cursor() as c:
        c.execute(f"UPDATE `{TABLE_NAME}` SET `done`=TRUE WHERE `id`=%s", (id_))
        db.commit()
    print(f"Task #{id_} closed")


if __name__ == "__main__":
    db = pymysql.connect("127.0.0.1", "root", "password", "")
    ensure_database(db)
    db.close()

    db = pymysql.connect("127.0.0.1", "root", "password", DATABASE_NAME)
    ensure_table(db)

    choice_functions = {"l": task_list, "a": add_task, "d": close_task}
    while True:
        print(
            "What do you want to do?:\n",
            "l: Show task list\n",
            "a: add a new task\n",
            "d: Mark task as done\n",
            "q: quit\n",
        )
        choice = input("> ").lower()
        if choice == "q":
            print("Closing...")
            break
        elif choice in choice_functions:
            choice_functions[choice](db)
        else:
            print(f"Unknown choice: '{choice}'. Try again.")
        print("----")
    db.close()
