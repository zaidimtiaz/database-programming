import pymysql
import datetime

db = pymysql.connect("127.0.0.1", "root", "password", "default")
with db.cursor() as c:
    c.execute(
        "DELETE FROM bikesharing WHERE tstamp BETWEEN %s AND %s",
        (datetime.datetime(2017, 1, 3), datetime.datetime(2017, 1, 3, 23, 59, 59)),
    )
    db.commit()
db.close()
