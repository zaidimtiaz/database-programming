import pymysql
import datetime

db = pymysql.connect("127.0.0.1", "root", "password", "default")
with db.cursor() as c:
    c.execute(
        "UPDATE bikesharing SET cnt = cnt+10 WHERE tstamp BETWEEN %s AND %s",
        (datetime.datetime(2015, 1, 9), datetime.datetime(2015, 1, 9, 23, 59, 59)),
    )
    db.commit()
db.close()
