import pymysql

db = pymysql.connect("127.0.0.1", "root", "password", "")

with db.cursor() as c:
    c.execute("SELECT VERSION()")
    version = c.fetchone()
    print(f"Database version: {version[0]}")
db.close()
