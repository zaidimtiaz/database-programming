# Database Programming

During this module, students will put their database knowledge to practice.
They will learn how to use database clients and ORMs to perform operations on
both SQL and NoSQL databases.

Some of the topics covered in this module:
- connecting to MySQL database
- PyMySQL client
- What is an ORM?
- SQLAlchemy ORM
- **\*** pymongo and operations on MongoDB

## Presentation
Presentation can be found [here](https://gitlab.com/sda-international/program/python/databases-programming/wikis/home).

## Further reading
- https://pymysql.readthedocs.io/en/latest/
- https://www.sqlalchemy.org/
- https://en.wikipedia.org/wiki/Object-relational_mapping
- https://api.mongodb.com/python/current/
